---
date: 2020-05-11T17:16:31.000+00:00
hero_image: "/content/images/greenhouse11.jpg"
title: GROUU Greenhouse
author: André Rocha

---
The GROUU greenhouse marks the first explorations of the project.

I was mainly transitioning from a curious designer using Arduino to applying it to a specific use. In the middle, I needed to combine on the same board, sensors, actuators, data collection, and connectivity. I didn’t get too deep automation nor strategies because I lost access to the place where this was setup.  
It was a very rich learning process and a deep synthesis of my last years as a designer. At the same time, a turning point in many ways.

I am now recollecting the documentation.